package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/asdine/storm"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	"github.com/go-chi/render"

	"gitlab.com/sacules/hangmanen2/backend/hangman"
)

// TODO: add a generic error response type

const (
	dbName         = `hangmanen.db`
	dbGameState    = `game state`
	dbGameCategory = `category`
	dbGameGuesses  = `guesses`
)

type server struct {
	// TODO: add a mutex?
	// TODO: add a logger?
	db     *storm.DB
	router *chi.Mux
}

// TODO: Init most config and game state here!
func setupDatabase(name string) (*storm.DB, func() error, error) {
	db, err := storm.Open(name)
	if err != nil {
		return nil, nil, fmt.Errorf("setup database: %v", err)
	}

	clean := func() error {
		return db.Close()
	}

	// setup game state

	// default value in case it doesn't exist
	var cat string
	err = db.Get(dbGameState, dbGameCategory, &cat)
	if err == storm.ErrNotFound {
		log.Println("creating a new game with default category (empty)...")
		err = db.Set(dbGameState, dbGameCategory, "")
	}
	if err != nil {
		return nil, nil, fmt.Errorf("setup database: %v", err)
	}

	g := hangman.Guesses{MaxTurns: 3}
	err = db.Get(dbGameState, dbGameGuesses, &g)
	if err == storm.ErrNotFound {
		log.Println("creating default empty guesses")
		err = db.Set(dbGameState, dbGameGuesses, &g)
	}
	if err != nil {
		return nil, nil, fmt.Errorf("setup database: %v", err)
	}

	log.Printf("loaded game category: '%v'", cat)

	return db, clean, nil
}

func setupRouter() *chi.Mux {
	// Allow connection from the web browser
	cors := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		AllowedMethods: []string{"GET", "POST", "DELETE"},
		AllowedHeaders: []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		MaxAge:         300,
	})

	router := chi.NewRouter()
	router.Use(
		render.SetContentType(render.ContentTypeJSON),
		middleware.Recoverer,
		cors.Handler,
		middleware.Logger,
	)

	return router
}

func newServer(db *storm.DB) *server {
	router := setupRouter()

	s := &server{db: db, router: router}

	return s
}

// Implement the httphandler interface
func (s *server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.router.ServeHTTP(w, r)
}

// FIXME: abstract these into a single one? worth it?
func (s *server) dbGetCategory() (string, error) {
	var cat string
	err := s.db.Get(dbGameState, dbGameCategory, &cat)
	if err != nil {
		return cat, fmt.Errorf("get category: %v", err)
	}

	return cat, nil
}

func (s *server) dbSetCategory(cat string) error {
	return s.db.Set(dbGameState, dbGameCategory, cat)
}

func (s *server) dbDeleteCategory() error {
	return s.dbSetCategory("")
}

func (s *server) dbCreateEntry(player, title string) error {
	model := entryModel{
		Entry: *hangman.NewEntry(title, player),
	}

	err := s.db.Save(&model)
	if err == storm.ErrAlreadyExists {
		return fmt.Errorf("entry already exists")
	}

	return err
}

func (s *server) dbGetGuesses() (hangman.Guesses, error) {
	var guesses hangman.Guesses
	err := s.db.Get(dbGameState, dbGameGuesses, &guesses)
	return guesses, err
}

func (s *server) dbSetGuesses(g hangman.Guesses) error {
	return s.db.Set(dbGameState, dbGameGuesses, &g)
}
