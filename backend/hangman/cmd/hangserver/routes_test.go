package main

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"strings"
	"testing"

	"github.com/go-chi/render"
	"github.com/matryer/is"

	"gitlab.com/sacules/hangmanen2/backend/hangman"
)

func TestHandleGameGet(t *testing.T) {
	is := is.New(t)

	testdb := "test handle game get.db"
	db, dbtidy, err := setupDatabase(testdb)
	is.NoErr(err)
	defer os.Remove(testdb)
	defer dbtidy()

	srv := newServer(db)
	r := httptest.NewRequest("GET", "/", nil)
	w := httptest.NewRecorder()

	srv.handleGameGet()(w, r)
	is.Equal(w.Code, http.StatusOK)

	// Actual response
	body, err := ioutil.ReadAll(w.Body)
	is.NoErr(err)
	respreal := string(body)

	// Mocked response
	mock := struct {
		Category string `json:"category"`
	}{
		Category: "",
	}
	render.JSON(w, r, &mock)
	body, err = ioutil.ReadAll(w.Body)
	is.NoErr(err)
	respmock := string(body)

	is.Equal(respreal, respmock)
}

func TestHandleGameCreate(t *testing.T) {
	is := is.New(t)

	testdb := "test handle game create.db"
	db, dbtidy, err := setupDatabase(testdb)
	is.NoErr(err)
	defer os.Remove(testdb)
	defer dbtidy()

	form := url.Values{}
	form.Add("category", "entry")
	postform := strings.NewReader(form.Encode())

	srv := newServer(db)
	w := httptest.NewRecorder()
	r := httptest.NewRequest("POST", "/", postform)
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	srv.handleGameCreate()(w, r)
	if w.Code != http.StatusCreated {
		body, err := ioutil.ReadAll(w.Body)
		is.NoErr(err)
		t.Errorf("expected code %d, got %d", http.StatusCreated, w.Code)
		t.Error(string(body))
	}

	// Test for game created!
}

func TestHandleGameDelete(t *testing.T) {
	is := is.New(t)

	testdb := "test handle game delete.db"
	db, dbtidy, err := setupDatabase(testdb)
	is.NoErr(err)
	defer os.Remove(testdb)
	defer dbtidy()

	srv := newServer(db)
	err = srv.dbSetCategory("foobar")
	is.NoErr(err)

	w := httptest.NewRecorder()
	r := httptest.NewRequest("DELETE", "/", nil)

	srv.handleGameDelete()(w, r)
	if w.Code != http.StatusOK {
		body, err := ioutil.ReadAll(w.Body)
		is.NoErr(err)
		t.Errorf("expected code %d, got %d", http.StatusCreated, w.Code)
		t.Error(string(body))
	}
}

func TestHandleEntriesGet(t *testing.T) {
	is := is.New(t)

	testdb := "test handle entries get.db"
	db, dbtidy, err := setupDatabase(testdb)
	is.NoErr(err)
	defer os.Remove(testdb)
	defer dbtidy()

	srv := newServer(db)
	err = srv.dbSetCategory("entry")
	is.NoErr(err)

	w := httptest.NewRecorder()
	r := httptest.NewRequest("GET", "/", nil)

	srv.handleEntriesGet()(w, r)
	is.Equal(w.Code, http.StatusOK)

	// Actual response
	body, err := ioutil.ReadAll(w.Body)
	is.NoErr(err)
	respreal := string(body)

	// Mocked response
	mock := []hangman.Entry{}
	render.JSON(w, r, &mock)
	body, err = ioutil.ReadAll(w.Body)
	is.NoErr(err)
	respmock := string(body)

	is.Equal(respreal, respmock)
}
