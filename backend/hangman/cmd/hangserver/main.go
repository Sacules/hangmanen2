package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/logrusorgru/aurora"
	"github.com/pkg/errors"
)

var au aurora.Aurora

func init() {
	au = aurora.NewAurora(true)
	log.SetPrefix("[" + fmt.Sprint(au.Bold(au.Magenta("hangserver"))) + "] ")
	log.SetFlags(log.Ltime)
}

func run() error {
	db, dbtidy, err := setupDatabase(dbName)
	if err != nil {
		return errors.Wrap(err, "setup database")
	}
	defer dbtidy()

	s := newServer(db)

	log.Println("setting up routes...")
	s.routes()

	// boot the server!
	log.Println("listening on port 4000")

	return http.ListenAndServe(":4000", s)
}

func main() {
	log.Println("starting the server...")
	if err := run(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
