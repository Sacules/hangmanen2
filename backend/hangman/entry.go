package hangman

// Entry represents a song, or album sent by a player.
type Entry struct {
	Player

	// Usually the song or album's name
	Title string `json:"title"`

	// This represents the title as a sequence of "_", ready to be filled
	BlankTitle string `json:"blank_title"`
}

// NewEntry generates an Entry object with given parameters.
func NewEntry(title, player string) *Entry {
	return &Entry{
		Player:     Player{Name: player, Guessed: false},
		Title:      title,
		BlankTitle: CreateBlankName(title),
	}
}

// GuessLetter tries to fill all instances of the given letter
// in the entry's BlankTitle. If it's already been guessed, nothing happens.
func (e *Entry) GuessLetter(l string) {
	e.BlankTitle = ReplaceLetter(e.Title, e.BlankTitle, l)
}

// GuessWord tries to fill the given word in the entry's BlankTitle.
// Only replaces the first instance of the word if it exists.
func (e *Entry) GuessWord(w string) {
	e.BlankTitle = ReplaceWord(e.Title, e.BlankTitle, w)
}
