package hangman

// Is it an entry round or an EP round?
const (
	GameEntry = `entry`
	GameEP    = `ep`
)

// TODO: deprecate this, we don't need it in this package,
// it can be saved on the db and returned as a custom
// response type

// Game represents the currently running game
type Game struct {
	Category string `json:"category"`
}
