package hangman

// EP holds plenty of entries while also being an entry itself.
type EP struct {
	Entry

	Entries []*Entry `json:"entries"`
}

// NewEP generates an EP object with given parameters.
func NewEP(name, player string) *EP {
	return &EP{
		Entry: *NewEntry(name, player),
	}
}

// Add appends an entry to the EP
func (ep *EP) Add(e *Entry) {
	ep.Entries = append(ep.Entries, e)
}

// GuessLetter tries to fill all instance of the given letter
// in all the entries' BlankName.
func (ep *EP) GuessLetter(l string) {
	for _, entry := range ep.Entries {
		entry.GuessLetter(l)
	}
}

// GuessWord tries to fill the given word in all the entries' BlankName.
// Only replaces the first instance of the word per entry if it exists.
func (ep *EP) GuessWord(w string) {
	for _, entry := range ep.Entries {
		entry.GuessWord(w)
	}
}
