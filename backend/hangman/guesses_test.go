package hangman

import (
	"os"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestNewGuesses(t *testing.T) {
	// No turns set
	var guesses = NewGuesses()

	if guesses.Letters == nil {
		t.Error("guesses' letters slice is nil instead of empty")
	}

	if guesses.Words == nil {
		t.Error("guesses' words slice is nil instead of empty")
	}

	if guesses.Players == nil {
		t.Error("guesses' players slice is nil instead of empty")
	}

	if guesses.MaxTurns != 3 {
		t.Error("max turns not set to 3")
	}

	guesses = NewGuesses(5)

	if guesses.MaxTurns != 5 {
		t.Error("max turns not set to 5")
	}
}

func TestRepeatedLetter(t *testing.T) {
	var (
		g      = new(Guesses)
		letter = "p"
	)

	if g.RepeatedLetter("") {
		t.Errorf("repeatedLetter: failed with empty letter")
	}

	g.AddLetter("l")
	g.AddLetter("p")
	g.AddLetter("m")
	g.AddLetter("q")
	g.AddLetter("t")

	if g.RepeatedLetter("z") {
		t.Errorf("repeatedLetter: letter found")
	}

	if !g.RepeatedLetter(letter) {
		t.Errorf("repeatedLetter: letter not found")
	}
}

func TestRepeatedWord(t *testing.T) {
	var (
		g    = new(Guesses)
		word = "Burial"
	)

	if g.RepeatedWord("") {
		t.Errorf("RepeatedWord: failed with empty word")
	}

	g.AddWord("Anathema")
	g.AddWord("Burial")
	g.AddWord("Massive Attack")

	if g.RepeatedWord("Portishead") {
		t.Errorf("RepeatedWord: word found")
	}

	if !g.RepeatedWord(word) {
		t.Errorf("RepeatedWord: word not found")
	}
}

func TestLoadGuesses(t *testing.T) {
	var (
		filename = "/tmp/guesses1.json"
		g1       = new(Guesses)
		g2       = new(Guesses)
	)

	g1.AddLetter("l")
	g1.AddLetter("o")
	g1.AddLetter("s")
	g1.AddLetter("s")

	g1.AddWord("Porcupine Tree")
	g1.AddWord("Haken")
	g1.AddWord("In the Name of")

	err := g1.Save(filename)
	defer os.Remove(filename)
	if err != nil {
		t.Error(err)
	}

	err = g2.Load(filename)
	if err != nil {
		t.Error(err)
	}

	if !cmp.Equal(g1, g2) {
		t.Fatal("load guesses: read guess is different from saved one")
	}
}

func TestAddPlayer(t *testing.T) {
	// Standard 3 turns
	var (
		guesses = NewGuesses()
		players = []Player{
			{"Sacules", false},
			{"Parama", false},
			{"Evermind", false},
		}
	)

	guesses.AddPlayer(Player{"Sacules", false})
	guesses.AddPlayer(Player{"Parama", false})
	guesses.AddPlayer(Player{"Evermind", false})

	if !cmp.Equal(guesses.Players, players) {
		t.Error("expected", players, "received", guesses.Players)
	}

	players = []Player{
		{"Parama", false},
		{"Evermind", false},
		{"TAC", false},
	}
	guesses.AddPlayer(Player{"TAC", false})

	if !cmp.Equal(guesses.Players, players) {
		t.Error("expected", players, "received", guesses.Players)
	}

	players = []Player{
		{"Evermind", false},
		{"TAC", false},
		{"jingle.boy", false},
	}
	guesses.AddPlayer(Player{"jingle.boy", false})

	if !cmp.Equal(guesses.Players, players) {
		t.Error("expected", players, "received", guesses.Players)
	}

	// 1 turn
	guesses = NewGuesses(1)
	players = []Player{
		{"Parama", false},
	}

	guesses.AddPlayer(Player{"Sacules", false})
	guesses.AddPlayer(Player{"Parama", false})

	if !cmp.Equal(guesses.Players, players) {
		t.Error("expected", players, "received", guesses.Players)
	}

	// 0 turns
	guesses = NewGuesses(0)
	players = []Player{
		{"Sacules", false},
	}

	guesses.AddPlayer(Player{"Sacules", false})

	if !cmp.Equal(guesses.Players, players) {
		t.Error("expected", players, "received", guesses.Players)
	}
}
