package hangman

import (
	"gitlab.com/Sacules/jsonfile"
)

// Guesses keeps track of the many guesses players make during the game.
type Guesses struct {
	Letters  []string `json:"letters"`
	Words    []string `json:"words"`
	Players  []Player `json:"players_guessing"`
	MaxTurns int      `json:"max_turns"`
}

// NewGuesses returns a pointer to a Guesses type, with the given MaxTurns.
// If missing or given 0, it defaults to 3.
func NewGuesses(maxturns ...uint) *Guesses {
	var turns uint

	if len(maxturns) == 0 || maxturns[0] == 0 {
		turns = 3
	} else {
		turns = maxturns[0]
	}

	return &Guesses{
		Letters:  make([]string, 0),
		Words:    make([]string, 0),
		Players:  make([]Player, 0, turns),
		MaxTurns: int(turns),
	}
}

// AddLetter to the list of guesses.
func (g *Guesses) AddLetter(l string) {
	g.Letters = append(g.Letters, l)
}

// AddPlayer a player to the end of a turn queue, and removes the first one.
func (g *Guesses) AddPlayer(p Player) {
	if len(g.Players) == g.MaxTurns {
		g.pop()
	}

	g.push(p)
}

// AddWord to the list of guesses.
func (g *Guesses) AddWord(w string) {
	g.Words = append(g.Words, w)
}

// Load all the guesses from a JSON file.
func (g *Guesses) Load(filename string) error {
	return jsonfile.Load(g, filename)
}

// RepeatedLetter checks if a given letter exists in the list of guesses.
func (g *Guesses) RepeatedLetter(l string) bool {
	for _, guess := range g.Letters {
		if guess == l {
			return true
		}
	}
	return false
}

// RepeatedWord checks if a word exists in the list of guesses.
func (g *Guesses) RepeatedWord(w string) bool {
	for _, guess := range g.Words {
		if guess == w {
			return true
		}
	}
	return false
}

// Save all the guesses to a JSON file.
func (g *Guesses) Save(filename string) error {
	return jsonfile.Save(g, filename)
}

func (g *Guesses) pop() {
	var newplayers = make([]Player, len(g.Players)-1)
	copy(newplayers, g.Players[1:])
	g.Players = newplayers
}

func (g *Guesses) push(p Player) {
	g.Players = append(g.Players, p)
}
