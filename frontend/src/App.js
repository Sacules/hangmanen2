import React from "react";
import logo from "./logo.svg";
import Hangman from "./Hangman.jsx";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Hangman />
      </header>
    </div>
  );
}

export default App;
